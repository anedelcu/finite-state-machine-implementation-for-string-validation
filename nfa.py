import copy
import sys
import os.path

__author__ = 'Alex Nedelcu'


class NFA():
    def __init__(self, file_input):
        self.noStates = 0
        self.noTransitions = 0
        self.transList = TransitionList()
        self.finalState = None
        self.startState = None
        self.fileInput = file_input
        self.words = []
        self.recEpsilon = []

        self.read()

    # reads the input file and loads all the transitions in a TransitionList object
    # also the words are loaded too, in case the simulation is being called
    def read(self):
        f = open(self.fileInput, 'r')
        self.noStates = int(f.readline().strip())
        self.finalState = int(self.noStates) - 1
        self.noTransitions = int(f.readline().strip())

        for i in range(0, self.noTransitions):
            transition = f.readline().strip()[1:-1]
            params = transition.split(",", 2)
            state = params[0]
            inp = params[1]

            if self.startState == None:
                self.startState = state
            nextStateList = params[2][1:-1].split(",")
            for ns in nextStateList:
                t = Transition(state, inp, ns)
                self.transList.addTransition(t)
                self.finalState = ns

        for line in f:
            if line.strip() != "":
                self.words.append(line.strip())

        f.close()


    # builds the content of the DOT file, based on the .input file
    def exportToGraph(self):

        result = ""

        result = "digraph fsm {"
        result = result + "\nrankdir=\"LR\""
        result = result + "\nstart [shape=\"plaintext\",label=\"start\"]"

        for s in range(0, self.noStates):
            if int(s) == int(self.finalState):
                shape = "doublecircle"
            else:
                shape = "circle"

            result = result + "\n" + str(s) + " [shape=\"" + shape + "\", label=\"S" + str(s) + "\"]"

        result = result + "\nstart->" + self.startState

        for t in self.transList.getList():
            result = result + "\n" + t.getState() + "->" + t.getFinalState() + " [label=\"" + t.getInput() + "\"]"

        result = result + "\n}"

        fo = open(self.fileInput[0:self.fileInput.rfind('.')] + ".dot", "w+")
        fo.write(result)
        fo.close()


    # checks a string against a NFA
    #
    # @param inx - it represents the start position of the regex - should be set to 0
    # @param textToCheck - string that need to be verified against the NFA
    # @param parentNode - the root of the tree that is being created when validating the textToCheck. - set this to None
    # @param presentState - when simulate is called, this should be set to the start state of the NFA
    # @param trace - keeps track of all state pattern of the match. Initialize this with an empty list.

    def simulate(self, inx, textToCheck, parentNode, presentState, trace):

        transList = self.transList

        # get the next possible states (children of this node)
        nextPossibleStates = transList.getNextStates(presentState, textToCheck[inx:inx+1])

        #build a node and their children based on the transition graph
        n = Node(parentNode, presentState)
        n.addChildren(nextPossibleStates)

        # this will reset the EPSILON transition counter when simulate is called a second time, with a different input
        if(parentNode == None):
            self.recEpsilon = []

        # if the end of the string was reached
        # return True, if the present state is also a final state
        # return False, if the present state is not a final state
        if inx is len(textToCheck):
            if a.finalState is presentState:
                trace.insert(0, presentState)
                return True
            else:
                return False

        # results list
        results = []

        initRecEpsilon = copy.deepcopy(self.recEpsilon)
        for state in nextPossibleStates:
            #print transList.getTransaction(presentState, state)
            #print presentState, state
            self.recEpsilon = copy.deepcopy(initRecEpsilon)

            if transList.getTransaction(presentState, state.label) != None:


                # if it is an EPSILON, do not increase the index in textToCheck
                if transList.getTransaction(presentState, state.label).getInput() == "EPSILON":
                    #in order to avoid EPSILON loops:
                    ok = True
                    for prevState in self.recEpsilon:
                        if prevState == presentState:
                            ok = False
                            self.recEpsilon = []

                    self.recEpsilon.append(presentState)

                    # go on the EPSILON route
                    if ok == True:
                        rz = self.simulate(inx, textToCheck, n, state.label, trace)

                    else:
                        rz = False

                else:
                    self.recEpsilon = []    #recEpsilon keeps track of the consecutive EPSILON transitions; it is used to avoid infinite EPSILON loops
                    rz = self.simulate(inx+1, textToCheck, n, state.label, trace)
            else:
                rz = False

            results.append(rz)

            # do not look for any other patterns if one is found
            if rz is True:
                break

        # return true if any of the check of the children matched the text
        for r in results:
            if r is True:
                trace.insert(0, presentState)
                return True
        return False

# stores a transition's initial state, input and nextState
class Transition():
    def __init__(self, initState, inp, finalState):
        self.initState = initState
        self.inp = inp
        self.finalState = finalState

    def getState(self):
        return self.initState

    def getInput(self):
        return self.inp

    def getFinalState(self):
        return self.finalState


# stores the transition list of a NFA
# it also provides some helpful methods to interacts with the NFA, such as
# addTransition, getNextStates(initState, input), getTransaction(initState, finalState)
# and getList()

class TransitionList():
    def __init__(self):
        self.L = []

    def addTransition(self, t):
        trans = self.getTransaction(t.getState(), t.getFinalState())
        if trans == None:
            self.L.append(t)
        else:
            trans.inp = trans.inp + "," + t.getInput()

    def getNextStates(self, initState, inp):
        r = []



        for t in self.L:
            # since the inputs are separated by comma, I am splitting them in an array
            inputs = t.getInput().split(',')
            for i in inputs:

                if t.getState() == initState:
                    if i == inp:
                        #r.append(list(t.getState(), t.getFinalState()))
                        r.append(t.getFinalState())

                    if i == "EPSILON":
                        #r.extend(self.getNextStates(t.getFinalState(), inp))
                        r.append(t.getFinalState())


        return r

    def getTransaction(self, initState, finalState):
        for t in self.L:
            if t.getState() == initState and t.getFinalState() == finalState:
                return t
        return None

    def getList(self):
        return self.L

# helper class for the NFA.simulate() method. It is based on trees
class Node():
    def __init__(self, parent, label):
        self.parent = parent
        self.label = label
        self.visited = False

    def addChildren(self, childrenList):
        self.childrenList = childrenList
        self.childrenList = []

        i=0
        for child in childrenList:
            c = Node(self, childrenList[i])
            childrenList[i] = c
            i = i + 1

    def getParent(self):
        return self.parent

    def getNextUnvisitedNode(self):
        for child in self.childrenList:
            if child.visited is False:
                return child



# check the number of arguments
if len(sys.argv)<3:
    print "Not enough arguments passed"
    exit(0)

#  get the params
filename_input = sys.argv[1]
action = sys.argv[2]

# loads the NFA
a = NFA(filename_input)

# execute desired action
if action == "graph":
    a.exportToGraph();
if action == "sim":
    for testValue in a.words:
        p = []
        success = a.simulate(0, testValue, None, "0", p)
        if success == True:
            print testValue, "is accepted. FDA Path:", " -> ".join(p)
        else:
            print testValue, "is not accepted"
