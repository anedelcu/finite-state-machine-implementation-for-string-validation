# This is a sample makefile file.  For the sake of example I compile
# graph.out from graph.c .  Just copy it from my directory on tux.  This
# will, of course, be different if you're using other languages (Python
# won't be compiled at all).
#
# I will fill in a shell variable ($inFile) and call make, so that I can see
# your stuff run w/various inputs that *I* provide
#

py=python	#python interpreter

.PHONY : clean run

	# graph is the target that runs your Part I against my input.  graph.out
	# is, in this example, the executable, compiled from graph.c (below) NOTE:
	# Do NOT define inFile inside of this file!  I will fill it in in the
	# environment

graph : nfa.py
	$(py) nfa.py $(InFile) graph
	dot -Tpng $(InFile:.input=.dot) > $(InFile:.input=.png)

sim : nfa.py
	$(py) nfa.py $(InFile) sim

clean :
	-\rm *.dot
	-\rm *.png

eraserestults :
	-\rm *.dot
	-\rm *.png
	
run : nfa.py
	$(py) nfa.py $(InFile) graph
	$(py) nfa.py $(InFile) sim